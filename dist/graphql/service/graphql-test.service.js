"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var aws_appsync_1 = __importStar(require("aws-appsync"));
var GraphQLTestService = /** @class */ (function () {
    function GraphQLTestService($url, $region, $authUrl, $username, $password, $application) {
        this.url = $url;
        this.region = $region;
        this.authUrl = $authUrl;
        this.username = $username;
        this.password = $password;
        this.application = $application;
    }
    GraphQLTestService.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var rawResponse, content;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.token) return [3 /*break*/, 3];
                        return [4 /*yield*/, fetch(this.authUrl + '/login', {
                                method: 'POST',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({
                                    email: this.username,
                                    password: this.password,
                                    app: this.application
                                })
                            })];
                    case 1:
                        rawResponse = _a.sent();
                        return [4 /*yield*/, rawResponse.json()];
                    case 2:
                        content = _a.sent();
                        this.token = content.response.idToken;
                        _a.label = 3;
                    case 3: return [2 /*return*/, this.token];
                }
            });
        });
    };
    /**
     * Use this method to test an AppSync GraphQL Query.
     *
     * Example:
     *
     * graphqlService.query(
     *   gql`query AllTrainings {
     *     allTrainings {
     *       id
     *       name
     *       level
     *       specialization
     *     }
     *   }`,
     *   done,
     *   (data) => {
     *     expect(data.allTrainings).that.contains.something.like({name: 'Test Unit'})
     *   }
     * )
     *
     * @param graphqlQuery The query to be executed.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    GraphQLTestService.prototype.query = function (graphqlQuery, done, validate) {
        var _this = this;
        this.getAppSyncClient().then(function (appSyncClient) {
            appSyncClient.hydrated().then(function (client) {
                client.query({
                    fetchPolicy: 'no-cache',
                    query: graphqlQuery
                }).then(function (response) {
                    _this.validateGraphqlResponse(response, done, validate);
                }, function (error) {
                    console.error(error);
                    done(error);
                }).catch(function (error) {
                    console.error(error);
                    done(error);
                });
            });
        });
    };
    /**
     * Use this method to test an AppSync GraphQL Query.
     *
     * @param graphqlQuery The query to be executed.
     * @param variables The Object contained the variables passed to the query as values.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    GraphQLTestService.prototype.queryVar = function (graphqlQuery, variables, done, validate) {
        var _this = this;
        this.getAppSyncClient().then(function (appSyncClient) {
            appSyncClient.hydrated().then(function (client) {
                client.query({
                    fetchPolicy: 'no-cache',
                    query: graphqlQuery,
                    variables: variables
                }).then(function (response) {
                    _this.validateGraphqlResponse(response, done, validate);
                }, function (error) {
                    console.error(error);
                    done(error);
                }).catch(function (error) {
                    console.error(error);
                    done(error);
                });
            });
        });
    };
    /**
     * Use this method to test an AppSync GraphQL Mutation.
     *
     * @param graphqlMutation The mutation to be executed.
     * @param variables The Object contained the variables passed to the mutation as values.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    GraphQLTestService.prototype.mutation = function (graphqlMutation, variables, done, validate) {
        var _this = this;
        this.getAppSyncClient().then(function (appSyncClient) {
            appSyncClient.hydrated().then(function (client) {
                client.mutate({
                    fetchPolicy: 'no-cache',
                    mutation: graphqlMutation,
                    variables: variables
                }).then(function (response) {
                    _this.validateGraphqlResponse(response, done, validate);
                }, function (error) {
                    console.error(error);
                    done(error);
                }).catch(function (error) {
                    console.error(error);
                    done(error);
                });
            });
        });
    };
    GraphQLTestService.prototype.validateGraphqlResponse = function (response, done, validate) {
        if (response.errors && response.errors.length > 0) {
            done(response.errors);
        }
        var data = response.data;
        try {
            validate(data);
        }
        catch (err) {
            console.error(err);
            done(err);
            return;
        }
        done();
    };
    GraphQLTestService.prototype.getAppSyncClient = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (!this.appSyncClient) {
                    this.appSyncClient = new aws_appsync_1.default({
                        url: this.url,
                        region: this.region,
                        auth: {
                            type: aws_appsync_1.AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
                            jwtToken: function () { return _this.login(); }
                        },
                        disableOffline: true
                    });
                }
                return [2 /*return*/, this.appSyncClient];
            });
        });
    };
    return GraphQLTestService;
}());
exports.GraphQLTestService = GraphQLTestService;
//# sourceMappingURL=graphql-test.service.js.map