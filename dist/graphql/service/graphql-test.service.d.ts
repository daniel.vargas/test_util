/// <reference types="mocha" />
export declare class GraphQLTestService {
    private url;
    private region;
    private authUrl;
    private username;
    private password;
    private application;
    private token;
    private appSyncClient;
    constructor($url: string, $region: string, $authUrl: string, $username: string, $password: string, $application: string);
    login(): Promise<string>;
    /**
     * Use this method to test an AppSync GraphQL Query.
     *
     * Example:
     *
     * graphqlService.query(
     *   gql`query AllTrainings {
     *     allTrainings {
     *       id
     *       name
     *       level
     *       specialization
     *     }
     *   }`,
     *   done,
     *   (data) => {
     *     expect(data.allTrainings).that.contains.something.like({name: 'Test Unit'})
     *   }
     * )
     *
     * @param graphqlQuery The query to be executed.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    query(graphqlQuery: any, done: Mocha.Done, validate: (data: any) => void): void;
    /**
     * Use this method to test an AppSync GraphQL Query.
     *
     * @param graphqlQuery The query to be executed.
     * @param variables The Object contained the variables passed to the query as values.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    queryVar(graphqlQuery: any, variables: any, done: Mocha.Done, validate: (data: any) => void): void;
    /**
     * Use this method to test an AppSync GraphQL Mutation.
     *
     * @param graphqlMutation The mutation to be executed.
     * @param variables The Object contained the variables passed to the mutation as values.
     * @param done The mocha done object that will be used to register failures.
     * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
     */
    mutation(graphqlMutation: any, variables: any, done: Mocha.Done, validate: (data: any) => void): void;
    private validateGraphqlResponse;
    private getAppSyncClient;
}
//# sourceMappingURL=graphql-test.service.d.ts.map