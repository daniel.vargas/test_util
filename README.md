# test_util

## Usage:

### Dependencies:

```
"@types/chai": "^4.2.9",
"@types/chai-like": "^1.1.0",
"@types/chai-subset": "^1.3.3",
"@types/chai-things": "0.0.34",
"@types/mocha": "^7.0.1",
"@types/node-fetch": "^2.5.4",
"aws-appsync": "^3.0.2",
"chai": "^4.2.0",
"chai-like": "^1.1.1",
"chai-subset": "^1.6.0",
"chai-things": "^0.2.0",
"cross-fetch": "^3.0.4",
"graphql": "^0.11.7",
"graphql-tag": "^2.10.3",
"mocha": "^7.0.1",
"mocha-teamcity-reporter": "^3.0.0",
"node-fetch": "^2.6.0",
"test_util": "git@gitlab.com:daniel.vargas/test_util.git#master"
```

### Scripts:

```
"test": "mocha -r ts-node/register test/**/*.spec.ts",
"test_tc": "mocha --reporter mocha-teamcity-reporter -r ts-node/register test/**/*.spec.ts",
```

### Test Example:

```
import 'mocha';
import aws_exports from '../aws-exports';
import { expect, assert, Assertion, use, should } from 'chai';
import {  } from "chai-subset";
import gql from "graphql-tag";
import 'cross-fetch/polyfill';
import chaiSubset = require('chai-subset');
import chaiLike = require('chai-like');
import chaiThings = require('chai-things');
import { GraphQLTestService } from "test_util";

describe("Example Test", () => {
  use(chaiSubset)
  use(chaiLike)
  use(chaiThings)
  let awsClient
  it("Should query ", function(done) {

    let graphqlService:GraphQLTestService = new GraphQLTestService(
      aws_exports.aws_appsync_graphqlEndpoint,
      aws_exports.aws_project_region,
      aws_exports.account_base_url,
      aws_exports.username,
      aws_exports.password,
      aws_exports.application_name
    )
    graphqlService.query(
      gql`query AllTrainings {
        allTrainings {
          id
          name
          level
          specialization
        }
      }`,
      done,
      (data) => {
        expect(data.allTrainings).that.contains.something.like({name: 'Test Unit'})
      }
    )
  })
})
```

### Recommended schema:

```
<Project>/
  test/
    graphql/
      query/
      mutation/
    fixtures/
    <test_folders>
```
