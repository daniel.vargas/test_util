import { GraphQLTestService } from "../../../src/graphql/service/graphql-test.service";
import awsexport from '../../aws-exports';
import 'mocha';
import { expect, assert, Assertion, use, should } from 'chai';
import {  } from "chai-subset";
import 'cross-fetch/polyfill';
import gql from "graphql-tag";
import chaiSubset = require('chai-subset');
import chaiLike = require('chai-like');
import chaiThings = require('chai-things');

describe('First test', 
  () => { 
    use(chaiSubset)
    use(chaiLike)
    use(chaiThings)
    it('should return true', function(done) { 
      let graphqlService:GraphQLTestService = new GraphQLTestService(
        awsexport.aws_appsync_graphqlEndpoint,
        awsexport.aws_appsync_region,
        awsexport.account_base_url,
        'daniel.vargas@dreamcodesoft.com',
        'Da1234567',
        'ECCO'
      )

      graphqlService.query(
        gql`query AllTrainings {
          allTrainings {
            id
            name
            level
            specialization
          }
        }`, 
        done,
        (data) => {
          console.info(data)
        })
  }); 
});