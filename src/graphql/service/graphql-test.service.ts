import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
import { NormalizedCacheObject } from "apollo-cache-inmemory";
import { ApolloQueryResult } from "apollo-client";

export class GraphQLTestService {
  private url:string;
  private region:string;
  private authUrl:string;
  private username:string;
  private password:string;
  private application:string;

  private token: string;
  private appSyncClient!: AWSAppSyncClient<NormalizedCacheObject>;

	constructor(
    $url: string,
    $region: string,
    $authUrl: string,
    $username: string,
    $password: string,
    $application: string) {
		this.url = $url;
		this.region = $region;
		this.authUrl = $authUrl;
		this.username = $username;
		this.password = $password;
    this.application = $application;
	}
	
  public async login():Promise<string> {
    if (!this.token) {
      const rawResponse = await fetch(this.authUrl + '/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: this.username,
          password: this.password,
          app: this.application
        })
      });
      const content = await rawResponse.json();
      this.token = content.response.idToken;
    }

    return this.token;
  }

  /**
   * Use this method to test an AppSync GraphQL Query.
   * 
   * Example:
   * 
   * graphqlService.query(
   *   gql`query AllTrainings {
   *     allTrainings {
   *       id
   *       name
   *       level
   *       specialization
   *     }
   *   }`,
   *   done,
   *   (data) => {
   *     expect(data.allTrainings).that.contains.something.like({name: 'Test Unit'})
   *   }
   * )
   * 
   * @param graphqlQuery The query to be executed.
   * @param done The mocha done object that will be used to register failures.
   * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
   */
  public query(graphqlQuery:any, done:Mocha.Done, validate:(data:any) => void) {
    this.getAppSyncClient().then(appSyncClient => {
      appSyncClient.hydrated().then(client => {
        client.query({
          fetchPolicy: 'no-cache',
          query: graphqlQuery
        }).then(response => {
          this.validateGraphqlResponse(response, done, validate)
        },
        (error) => {
          console.error(error)
          done(error)
        }).catch(error => {
          console.error(error)
          done(error)
        })
      })
    })
  }

  /**
   * Use this method to test an AppSync GraphQL Query.
   * 
   * @param graphqlQuery The query to be executed.
   * @param variables The Object contained the variables passed to the query as values.
   * @param done The mocha done object that will be used to register failures.
   * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
   */
  public queryVar(graphqlQuery:any, variables:any, done:Mocha.Done, validate:(data:any) => void) {
    this.getAppSyncClient().then(appSyncClient => {
      appSyncClient.hydrated().then(client => {
        client.query({
          fetchPolicy: 'no-cache',
          query: graphqlQuery,
          variables: variables
        }).then(response => {
          this.validateGraphqlResponse(response, done, validate)
        },
        (error) => {
          console.error(error)
          done(error)
        }).catch(error => {
          console.error(error)
          done(error)
        })
      })
    })
  }

  /**
   * Use this method to test an AppSync GraphQL Mutation.
   * 
   * @param graphqlMutation The mutation to be executed.
   * @param variables The Object contained the variables passed to the mutation as values.
   * @param done The mocha done object that will be used to register failures.
   * @param validate Function that will get called once a successfull response is obtained with the given query. Validate the data using this function.
   */
  public mutation(graphqlMutation:any, variables:any, done:Mocha.Done, validate:(data:any) => void) {
    this.getAppSyncClient().then(appSyncClient => {
      appSyncClient.hydrated().then(client => {
        client.mutate({
          fetchPolicy: 'no-cache',
          mutation: graphqlMutation,
          variables: variables
        }).then(response => {
          this.validateGraphqlResponse(response, done, validate)
        },
        (error) => {
          console.error(error)
          done(error)
        }).catch(error => {
          console.error(error)
          done(error)
        })
      })
    })
  }

  private validateGraphqlResponse(response:ApolloQueryResult<unknown>, done:Mocha.Done, validate:(data:any) => void) {
    if (response.errors && response.errors.length > 0) {
      done(response.errors)
    }

    let data:any = response.data
    try {
      validate(data)
    } catch (err) {
      console.error(err)
      done(err) 
      return;
    }
    done()
  }

  private async getAppSyncClient():Promise<AWSAppSyncClient<NormalizedCacheObject>> {
    if (!this.appSyncClient) {
      this.appSyncClient = new AWSAppSyncClient({
        url: this.url,
        region: this.region,
        auth: {
          type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
          jwtToken: () => this.login()
        },
        disableOffline: true
      });
    }

    return this.appSyncClient;
  }
}